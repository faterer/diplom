FROM alpine AS builder
WORKDIR /app
RUN apk update && apk add npm
COPY . .
RUN npm install --include=dev && \
    npm audit fix && \ 
    npm run build 

FROM nginx:1.25-alpine3.18 AS runner
EXPOSE 80
COPY --from=builder /app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"] 
